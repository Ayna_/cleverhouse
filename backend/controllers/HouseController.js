const HouseService = require('../services/HouseService')
const Util = require('../utils/Utils')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const util = new Util()

class HouseController {
    static async get_houses_of_user(req, res) {
        const user = req.user

        try {
            const houses = await HouseService.get_houses_of_user(user.idUser)
            if (houses.length > 0) {
                util.setSuccess(200, 'Houses retrieved', houses)
            } else {
                util.setError(404, 'No house found')
            }
            return util.send(res)
        } catch (error) {
            util.setError(500, `Internal error`)
            return util.send(res)
        }
    }

    static async get_house(req, res) {
        const user = req.user
        const { id } = req.params

        try {
            const house = await HouseService.get_house(id, user.idUser)

            if (!house) {
                util.setError(404, `Cannot find house with the id ${id}`)
            } else {
                util.setSuccess(200, 'Found house', house)
            }
            return util.send(res)
        } catch (error) {
            util.setError(500, `Internal error`)
            return util.send(res)
        }
    }

    static async add_house(req, res) {
        const user = req.user
        const { nameHouse } = req.body

        if (!nameHouse) {
            util.setError(400, `Please provide a nameHouse`)
            return util.send(res)
        }

        try {
            const createdHouse = await HouseService.add_house(user.idUser, { nameHouse: nameHouse })
            if (createdHouse === 'name') {
                util.setError(409, 'House name already exists')
                return util.send(res)
            }
            util.setSuccess(201, 'House Added!', createdHouse)
            return util.send(res)
        } catch (error) {
            util.setError(500, `Cannot add house ${nameHouse}`)
            return util.send(res)
        }
    }

    static async add_user_to_house(req, res) {
        const user = req.user
        const { idHouse } = req.params
        const { idUser, isMain } = req.body

        if (!idUser) {
            util.setError(400, 'Please provide an idUser')
            return util.send(res)
        }

        try {
            const createdHouse = await HouseService.add_user_to_house(parseInt(idHouse), user.idUser, idUser, isMain)
            if (createdHouse === 'house') {
                util.setError(404, "House doesn't exist")
            } else if (createdHouse === 'rights') {
                util.setError(403, "You don't have the rights to add an user")
            } else if (createdHouse === 'exists') {
                util.setError(403, 'User already exists in this house')
            } else util.setSuccess(201, `User added to house !`, createdHouse)
            return util.send(res)
        } catch (error) {
            util.setError(500, `Cannot add user`)
            return util.send(res)
        }
    }

    static async update_house(req, res) {
        const user = req.user
        const alteredHouse = req.body
        const { idHouse } = req.params

        try {
            const updateHouse = await HouseService.update_house(idHouse, alteredHouse, user.idUser)
            if (!updateHouse) {
                util.setError(404, `Cannot find house with the id: ${idHouse}`)
            } else {
                util.setSuccess(200, 'House updated', updateHouse)
            }
            return util.send(res)
        } catch (error) {
            util.setError(500, `Internal error`)
            return util.send(res)
        }
    }

    static async delete_house(req, res) {
        const user = req.user
        const { idHouse } = req.params

        try {
            const houseToDelete = await HouseService.delete_house(idHouse, user.idUser)
            if (houseToDelete === 'rights') {
                util.setError(403, "User doesn't have rights in house")
            } else if (houseToDelete === 'house') {
                util.setError(404, `House with the idHouse ${idHouse} cannot be found`)
            } else {
                util.setSuccess(200, `House deleted ${idHouse}`)
            }
            return util.send(res)
        } catch (error) {
            util.setError(500, `Internal error`)
            return util.send(res)
        }
    }
}

module.exports = HouseController
