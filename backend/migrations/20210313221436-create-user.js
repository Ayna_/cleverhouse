'use strict'
module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable('Users', {
            idUser: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            email: {
                type: Sequelize.STRING(50),
                allowNull: false,
                unique: true,
            },
            username: {
                type: Sequelize.STRING(50),
                allowNull: false,
                unique: true,
            },
            password: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        })
        await queryInterface.createTable(
            'UserIsMain',
            {
                idUserIsMain: {
                    allowNull: false,
                    autoIncrement: true,
                    primaryKey: true,
                    type: Sequelize.INTEGER,
                },
                idUser: {
                    allowNull: false,
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'Users',
                        key: 'idUser',
                    },
                    onDelete: 'CASCADE',
                    onUpdate: 'CASCADE',
                },
                idHouse: {
                    allowNull: false,
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'Houses',
                        key: 'idHouse',
                    },
                    onDelete: 'CASCADE',
                    onUpdate: 'CASCADE',
                },
                isMain: {
                    type: Sequelize.BOOLEAN,
                    allowNull: false,
                    default: false,
                },
                createdAt: {
                    allowNull: false,
                    type: Sequelize.DATE,
                },
                updatedAt: {
                    allowNull: false,
                    type: Sequelize.DATE,
                },
            },
            {
                uniqueKeys: {
                    unique_tag: {
                        customIndex: true,
                        fields: ['idUser', 'idHouse'],
                    },
                },
            }
        )
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('Users')
        await queryInterface.dropTable('UserIsMain')
    },
}
