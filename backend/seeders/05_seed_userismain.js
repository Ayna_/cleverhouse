'use strict'

module.exports = {
    up: async (queryInterface, Sequelize) => {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        return await queryInterface.bulkInsert(
            'UserIsMain',
            [
                {
                    idUser: 1,
                    idHouse: 1,
                    isMain: true,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    idUser: 1,
                    idHouse: 2,
                    isMain: true,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    idUser: 2,
                    idHouse: 1,
                    isMain: false,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    idUser: 3,
                    idHouse: 3,
                    isMain: true,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
            ],
            {}
        )
    },

    down: async (queryInterface, Sequelize) => {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        return await queryInterface.bulkDelete('UserIsMain', null, {})
    },
}
