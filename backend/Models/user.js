const sequelize = require('sequelize')
const Sequelize = require('sequelize')

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        idUser: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
        },
        email: {
            type: Sequelize.STRING(50),
            allowNull: false,
            unique: true,
        },
        username: {
            unique: true,
            type: Sequelize.STRING(50),
            allowNull: false,
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false,
        },
    })
    User.addScope('noPassword', {
        attributes: { exclude: ['password'] },
    })
    User.associate = function (models) {
        User.hasMany(models.UserIsMain, {
            foreignKey: 'idUser',
            as: 'userIsMain',
            onDelete: 'CASCADE',
            hooks: true,
        })
        User.hasMany(models.UserDevices, {
            foreignKey: 'idUser',
            as: 'userDevice',
            onDelete: 'CASCADE',
            hooks: true,
        })
    }

    return User
}
