from typing import List


from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


class Room(BaseModel):
    idRoom: int
    roomName: str
