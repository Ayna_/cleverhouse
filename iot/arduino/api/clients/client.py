from pymongo import MongoClient
# pprint library is used to make the output look more pretty
from pprint import pprint
# connect to MongoDB, change the << MONGODB URL >> to reflect your own connection string
client = MongoClient("mongodb://cleverhouseadmin:cleverhouseadmin@clustercleverhouse-shard-00-00.m45vb.mongodb.net:27017,clustercleverhouse-shard-00-01.m45vb.mongodb.net:27017,clustercleverhouse-shard-00-02.m45vb.mongodb.net:27017/collectioncleverhouse?ssl=true&replicaSet=atlas-ikvb3c-shard-0&authSource=admin&retryWrites=true&w=majority")
db=client.test
# Issue the serverStatus command and print the results
serverStatusResult=db.command("serverStatus")
pprint(serverStatusResult)